import React, { useState } from 'react';
import { apiClient } from './Api';
import { Input } from './components/Input';
import './App.css';

const App: React.FC = () => {
  const [query, setQuery] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [posts, setPosts] = useState<any[]>([]);

  const handleSearch = async () => {
    setLoading(true);
    try {
      const response = await apiClient.fetchList();
      setPosts(response.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="App">
      <Input value={query} onChange={event => setQuery(event.target.value)} />
      <button onClick={handleSearch}>Search</button>
      {isLoading ? (
        <p>Loading...</p>
      ) : (
        // to juz by mogły być osobne komponenty (jakiś Loader i lista)
        <ul className="list">
          {posts.length > 0 &&
            posts.map(post => {
              return <li key={post.id}>{JSON.stringify(post)}</li>;
            })}
        </ul>
      )}
    </div>
  );
};

export default App;
