import axios, { AxiosInstance, AxiosResponse } from 'axios';

export class Api {
  client: AxiosInstance

  constructor(client?: AxiosInstance) {
    this.client = client || this.initializeClient();
  }

  initializeClient() {
    return axios.create({
      baseURL: 'https://jsonplaceholder.typicode.com'
    })
  }


  fetchList(): Promise<AxiosResponse<any>> {
    return this.client.get("/posts")
  }


  handleSearch(query: string): Promise<AxiosResponse<any>> {
    return this.client.get(`/todos/${query}`)
  }

}

export const apiClient = new Api();
